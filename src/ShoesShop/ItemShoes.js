import React, { Component } from 'react'

export default class ItemShoes extends Component {
    render() {

        let { image, name, description } = this.props.ShoesData

        return (
            <div className='col-3'>
                <div className="card" style={{ width: '100%' }}>
                    <img src={image} className="card-img-top" alt={name} />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">
                            {description.length < 30
                                ? description
                                : description.slice(0.3) + "..."}
                        </p>
                        <button
                            onClick={() => {
                                this.props.handleAddToCart(this.props.ShoesData)
                            }}
                            className="btn btn-primary">
                            Add to cart
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}
