import React, { Component } from 'react'
import { ShoesList } from './DataShoes'
import ItemShoes from './ItemShoes'
import TableCart from './TableCart'


export default class RenderShoes extends Component {

    state = {
        ShoesList: ShoesList,
        cart: [],
    }

    renderShoes = () => {
        return this.state.ShoesList.map(item => {
            return <ItemShoes
                handleAddToCart={this.handleAddToCart}
                ShoesData={item}
                key={item.id}
            />
        })
    }

    handleAddToCart = (product) => {
        let index = this.state.cart.findIndex((item) => {
            return item.id === product.id
        })

        let cloneCart = [...this.state.cart]

        if (index === -1) {
            // issue 1
            let newProduct = { ...product, quantity: 1 }
            cloneCart.push(newProduct)
        } else {
            // issue 2
            cloneCart[index].quantity++
        }

        this.setState({
            cart: cloneCart,
        })

    }

    handleRemoveShoes = (id) => {
        let index = this.state.cart.findIndex((item) => {
            return (item.id === id)
        })
        if (index !== -1) {
            let cloneCart = [...this.state.cart]
            cloneCart.splice(index, 1)
            this.setState({
                cart: cloneCart,
            })
        }
    }

    render() {
        return (
            <div className='container py-5'>
                {this.state.cart.length > 0 && (
                    <TableCart
                        handleRemoveShoes={this.handleRemoveShoes}
                        cart={this.state.cart}
                    />
                )}

                <div className="row">{this.renderShoes()}</div>
            </div>
        )
    }
}
