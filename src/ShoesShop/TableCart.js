import React, { Component } from 'react'

export default class TableCart extends Component {

    renderContent = () => {
        return this.props.cart.map((item) => {
            return (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>
                        <button className='btn btn-danger'>-</button>
                        <span className='mx-2'>{item.quantity}</span>
                        <button className='btn btn-success'>+</button>
                    </td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.handleRemoveShoes(item.id)
                            }}
                            className='btn btn-danger'>
                            Xóa
                        </button>
                    </td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <td>Id</td>
                            <td>Name</td>
                            <td>Price</td>
                            <td>Quantity</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderContent()}
                    </tbody>
                </table>
            </div>
        )
    }
}
