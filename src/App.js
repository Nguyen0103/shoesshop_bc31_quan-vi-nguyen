import logo from './logo.svg';
import './App.css';
import RenderShoes from './ShoesShop/RenderShoes';

function App() {
  return (
    <div className="App">
      <RenderShoes />
    </div>
  );
}

export default App;
